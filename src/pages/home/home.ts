import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Http} from '@angular/http';
import 'rxjs/Rx';

@IonicPage()

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  public randomProducts=[];
  public categories=[];
  public page:number=0;

  constructor(public navCtrl: NavController,  private http: Http) {
    this.getRandomProducts();
    this.getCategories();
  }

  onLoadSearch(){
    this.navCtrl.push('SearchPage');
  }

  onLoadSearchByCategory(id,name){
     console.log(id); console.log(name);
     this.navCtrl.push('SearchResultsPage',{navParam1: "category_id="+id, navParam2:name});
  }

  //sub-functions start
    getCategories():void{
      let url = "https://api.bukalapak.com/v2/categories.json";
      this.httpGet(url).subscribe(data => {
        for (let i = 0; i < data.categories.length; i++) {
          this.categories.push(data.categories[i]);
        }
        console.log(this.categories);
      });
    }
    getRandomProducts():void{
      this.page += 1;
      let per_page = 12;
      let url = "https://api.bukalapak.com/v2/products.json/?page="+this.page+"&per_page="+per_page;
      this.httpGet(url).subscribe(data => {
        // for (let i = 0; i < per_page; i++) {
        //   this.randomProducts.push(data.products[i]);
        // }
        this.randomProducts = data.products;
        console.log(this.randomProducts);
        // console.log(this.randomProducts[0]);
      });
    }
    httpGet(url:string){
       return this.http.get(url).map(res => {return res.json()});
    }
  //sub-functions end

}
