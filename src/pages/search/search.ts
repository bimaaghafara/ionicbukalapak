import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})

export class SearchPage {
  search = {keywords : ''};

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  searchByKeywords() {
    console.log(this.search.keywords);
    this.navCtrl.push("SearchResultsPage",{navParam1: "keywords="+this.search.keywords, navParam2:this.search.keywords});
  }

}
