import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http} from '@angular/http';
import 'rxjs/Rx';

@IonicPage()

@Component({
  selector: 'page-search-results',
  templateUrl: 'search-results.html',
})

export class SearchResultsPage {
  public searchProducts= [];
  public urlParam: any;
  public nameParam: any;
  public page=1;
  public url;

  constructor(public navCtrl: NavController,private http: Http, public navParams: NavParams) {
    this.urlParam = this.navParams.get('navParam1');
    this.nameParam = this.navParams.get('navParam2'); 
    console.log(this.urlParam); console.log(this.nameParam);
    this.getSearchProducts();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchResultsPage');
    // this.urlParam = this.navParams.get('navParam1');
    // this.nameParam = this.navParams.get('navParam2');  
    // let url;
    // url = "https://api.bukalapak.com/v2/products.json/?page=1&per_page=12&"+this.urlParam;
    // this.http.get(url).map(res => res.json())
    //   .subscribe(data => {this.searchProducts = data.products; console.log(this.searchProducts);});
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      this.getSearchProducts();
      infiniteScroll.complete();
    }, 500);
  }

  //sub-functions start
    getSearchProducts():void{
      this.page+=1;
      let per_page = 12;
      this.url = "https://api.bukalapak.com/v2/products.json/?page="+this.page+"&per_page="+per_page+"&"+this.urlParam;
      console.log(this.url);
      this.httpGet(this.url).subscribe(data => {
        for (let i = 0; i < per_page; i++) {
          this.searchProducts.push(data.products[i]);
        }
        // console.log(this.searchProducts);
      });
    }
    httpGet(url:string){
       return this.http.get(url).map(res => {return res.json()});
    }
  //sub-functions end

}
